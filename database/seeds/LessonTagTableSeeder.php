<?php
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use App\{Lesson, Tag};
class LessonTagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        $lessonIds = Lesson::get()->pluck('id')->toArray();
        $tagIds = Tag::get()->pluck('id')->toArray();

		foreach(range(1, 30) as $index) {
            DB::table('lesson_tag')->insert([
                'lesson_id' => $faker->randomElement($lessonIds),
                'tag_id' => $faker->randomElement($tagIds)
                ]);
		}
    }
}
