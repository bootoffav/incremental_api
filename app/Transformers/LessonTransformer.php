<?php

namespace App\Transformers;


class LessonTransformer extends Transformer {
	
    public function transform($item)
    {
            return [
                'title' => $item['title'],
                'body' => $item['body'],
                'active' => (boolean) $item['some_bool']
            ];
    }
}