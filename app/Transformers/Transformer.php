<?php

namespace App\Transformers;


abstract class Transformer {

	abstract public function transform($item);

    public function transformCollection($items)
    {
        return array_map([$this, 'transform'], $items);
    }
}