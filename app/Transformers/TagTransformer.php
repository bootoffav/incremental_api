<?php

namespace App\Transformers;


class TagTransformer extends Transformer {
	
    public function transform($item)
    {
            return [
                'name' => $item['name'],
            ];
    }
}