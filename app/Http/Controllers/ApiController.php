<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;

class ApiController extends Controller {

	protected $statusCode = 200;

	public function getStatusCode()
	{
		return $this->statusCode;
	}

	public function setStatusCode($statusCode)
	{
		$this->statusCode = $statusCode;

		return $this;
	}

	public function respond($data)
	{
		return response()->json($data, $this->getStatusCode());
	}

	public function responseNotFound($message = 'not found')
	{
		return $this->setStatusCode(Response::HTTP_NOT_FOUND)->respondWithError($message);
	}


	public function respondWithError($message)
	{
		return $this->respond([
    		'error' => [
    			'message' => $message,
    			'status_code' => $this->getStatusCode()
    		]
    	]);
	}

	public function respondCreated($message)
	{
        return $this->setStatusCode(Response::HTTP_CREATED)
            ->respond([
                'message' => $message
            ]);
	}

}