<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lesson;
use App\Transformers\LessonTransformer;

class LessonsController extends ApiController
{
    /**
     * @var  App\Transformers\LessonTransformer
     */
    protected $lessonTransformer;

    public function __construct(LessonTransformer $lessonTransformer)
    {
        $this->lessonTransformer = $lessonTransformer;
    }

    public function index()
    {
        return $this->respond([
            'data' => $this->lessonTransformer->transformCollection(
                Lesson::all()->toArray()
            )
        ]);
    }

    public function show($id)
    {
    	$lesson = Lesson::find($id);

    	if (!$lesson) {
            return $this->responseNotFound();  
    	}

        return $this->respond([
    		'data' => $this->lessonTransformer->transform($lesson)
    	]);
    }

    public function store(Request $request)
    {
        if (!$request->input('title') or !$request->input('body'))
        {
            return $this->setStatusCode(422)
                        ->respondWithError('Not authorized');
        }

        Lesson::create($request->all());

        return $this->respondCreated('Lesson successfully created');
    }
}
