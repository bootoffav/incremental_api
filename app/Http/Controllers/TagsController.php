<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{Tag, Lesson};
use App\Transformers\TagTransformer;

class TagsController extends ApiController
{
    protected $tagTransformer;

    public function __construct(TagTransformer $tagTransformer)
    {
    	$this->tagTransformer = $tagTransformer;
    }

    public function index($lessonId = null)
    {
	    $tags = $this->getTags($lessonId);
	    return $this->respond([
	    	'data' => $this->tagTransformer->transformCollection(
                $tags->toArray()
            )
        ]);
    }

    public function getTags($lessonId)
    {
    	return $lessonId ? Lesson::findOrFail($lessonId)->tags : Tag::all();
    }
}
